package np.com.vikashparajuli.otto;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import np.com.vikashparajuli.otto.bus.EventBus;

public class MainActivity extends AppCompatActivity {

    Bus bus = EventBus.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bus.register(this);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    @Subscribe
    public void getMessage(String s) {
        Toast.makeText(this, "@ MainActivity: " + s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.object:
                TestData t = new TestData();
                t.message = "From MainActivity with object";
                bus.post(t);
                break;
            case R.id.string:
                bus.post("From MainActivity with String");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
