package np.com.vikashparajuli.otto;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.otto.Bus;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import np.com.vikashparajuli.otto.bus.EventBus;

/**
 * Created by Vikash on 1/5/2016.
 */
public class PlaceholderFragment extends Fragment {

    Bus bus = EventBus.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        View button = rootView.findViewById(R.id.fragmentbutton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bus.post("Hello from the Fragment");
            }
        });

        try {
            bus.register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }

    @Subscribe
    public void getMessage(TestData data) {
        Toast.makeText(getActivity(), "@ Fragment: " + data.message, Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void getMessage(String data) {
        Toast.makeText(getActivity(), data, Toast.LENGTH_SHORT).show();
    }

    @Produce
    public String produceEvent() {
        return "Starting up";
    }
}
