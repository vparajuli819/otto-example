package np.com.vikashparajuli.otto.bus;

import com.squareup.otto.Bus;

public class EventBus {
    private static Bus instance = null;

    private EventBus() {
        instance = new Bus();
    }

    public static Bus getInstance() {
        if (instance == null) {
            instance = new Bus();
        }
        return instance;
    }
}
